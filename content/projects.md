+++
title = "Projects"
slug = "projects"
+++

---

## Lorentz Violations Research
> As a sophomore, I joined Dr. Matthew Mewes' research team working on Lorentz Violations. To broadly summarize the field, Lorentz violating physics research utilizes a theoretical framework known as the Standard Model extension (SME) to experimentally verify deviations from Lorentz invariance that may be a part of a unified theory of physics. As of now, I am working on a project where we analyze the Lorentz Violating decay of neutrinos into electron-positron pairs.

## Working with the VERITAS Collaboration
> When I first started as a freshman at Cal Poly, I was drawn toward experimental astrophysics. With Dr. Jodi Christiansen, I analyzed data from the VERITAS telescopes to detect and study blazars in distant galaxies. We worked on algorithms in python and perl to help aid analysis of high energy, Cherenkov events that the telescopes detected.

## Python Workshop
> During my time at Cal Poly, my friend and I stared up a workshop where we taught students how to use computers as tools for science. We planned a free 10 week course where we taught students how to write code in Python and use UNIX/Linux systems. Teaching students the basics of computer programming and operating systems was one of the highlights of my time at Cal Poly.

## Nixos and Linux
> About 4 or so years ago, I switched my operating system over to Arch Linux. I found a love for designing and building my own, personalized system. After a while, I switched over to NixOS and found many fun benefits to it. My current NixOS system flake is on GitLab at [this link](https://gitlab.com/coffeetablebrothers/higher). Please go over and give it a look, I'm always looking for advice!

## Works in Progress
> I am always looking for cool new projects to work on, so send me an email or message me and I'll be super excited to take a look and help. Currently I'm working on a Grad School Helper for Cal Poly students and a guide for the children of Hindu immigrants to help convince their parents against joining the rising tide of Hindu nationalism in India.
