+++
title = "Contact"
slug = "contact"
+++

---

Hello! If you want to contact me, you can send me an email, or a message on matrix!
  My email is pranav@myrdd.info and my matrix handle is [@rightleftspin:myrdd.info](). (If you ever want to find me on another social media application, chances are I'll be using the handle [@rightleftspin](), it's what I use for twitter, discord, and instagram too.)
