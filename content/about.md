+++
title = "About"
description = "Pranav Seetharaman About Page."
date = "2019-02-28"
aliases = ["about-us", "about-hugo", "contact"]
author = "Pranav Seetharaman"
+++

---

> #### Hello! My name is Pranav Seetharaman and I just graduated from Cal Poly SLO with a B.S in Physics. I spend most of my time in front of my whiteboard working out the solution to some captivating theoretical physics problem. When I'm not holding a whiteboard pen, I might be behind my computer writing code or messing with my operating system. Currently, I use a system called NixOS for both my personal use and as a server (actually, the server that this website is currently hosted on was configured in NixOS). I'm an Indian-American, first generation immigrant and have lived most of my life in the Bay Area.
> #### Aside from that, I love to design escape room puzzles, play the indian and western flute, and I'm an eagle scout. If you have some time, check out the projects I have and send me an email if you want!
